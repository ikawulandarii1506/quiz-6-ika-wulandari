<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS Sort</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>26d61dc7-0fe8-45be-9294-ea88e7af7eee</testSuiteGuid>
   <testCaseLink>
      <guid>05450655-c4ec-4c87-9777-4835c433ea96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product/TC Sort DDL</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>20e2d1f8-8513-4120-96e3-63c5770b93a9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/data sort</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>20e2d1f8-8513-4120-96e3-63c5770b93a9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>sort</value>
         <variableId>253ad039-7e0c-4356-9879-5ddd2bd41376</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
