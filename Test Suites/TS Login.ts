<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>5d803150-7435-46d0-be72-8cfdae1856af</testSuiteGuid>
   <testCaseLink>
      <guid>5aad309b-f874-4c1c-b36b-875531188cce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC Login Valid - DDL</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9237d27d-7d53-4e9c-9e29-ed37f297f719</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/data user</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>9237d27d-7d53-4e9c-9e29-ed37f297f719</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>6676bb39-0fcd-43ac-8e4a-fbbf40def9ca</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>9237d27d-7d53-4e9c-9e29-ed37f297f719</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>b581304c-cb41-4f23-a9ce-b8ee56daa099</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
